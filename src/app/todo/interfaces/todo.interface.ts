export interface ITodoItem {
  _id?: number;
  title: string;
  completed?: boolean;
}
