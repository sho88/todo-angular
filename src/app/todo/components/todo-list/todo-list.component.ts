import { TodoService } from '../../services/todo.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ITodoItem } from '../../interfaces/todo.interface';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  list$: Observable<ITodoItem[]>;
  number$: Observable<number>;
  value: string;

  constructor(private readonly _todoService: TodoService) { }

  ngOnInit(): void {
    this.list$ = this._todoService.getState();
  }

  // events go here...
  onAddItem(): void {
    if (!this.value) {
      return;
    }

    this._todoService.addItem({ title: this.value });
    this.value = '';
  }

  onRemove(item: ITodoItem): void {
    this._todoService.removeItem(item);
  }

  onTextChange(value): void {
    this.value = value;
  }

  onToggled(value: ITodoItem): void {
    this._todoService.toggleItem(value);
  }
}
