import { TodoListComponent } from './todo-list.component';
import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { TodoService } from '../../services/todo.service';
import { TodoInputComponent } from '../todo-input/todo-input.component';
import { FormsModule } from '@angular/forms';

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;
  let service: TodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, FormsModule],
      declarations: [TodoListComponent, TodoInputComponent],
    });

    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(TodoService);

    fixture.detectChanges();
  });

  describe('WHEN My component has loaded', () => {
    it('SHOULD load perfectly well.', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('WHEN I click "onAddItem"', () => {
    it('SHOULD add an item using my service', () => {
      spyOn(service, 'addItem');

      component.onAddItem();
      expect(service.addItem).toHaveBeenCalled();
      expect(component.value).toBe('');
    });
  });

  describe('WHEN I change the text', () => {
    it('SHOULD change the value as expected', () => {
      const value = 'Change value';
      component.onTextChange(value);
      expect(component.value).toBe(value);
    });
  });

});
