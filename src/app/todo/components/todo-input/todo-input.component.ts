import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.css']
})
export class TodoInputComponent {
  @Input() query: string;
  @Output() whenTextChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  // events go here...
  onModelChanges(value: string): void {
    this.whenTextChange.emit(value);
  }

}
