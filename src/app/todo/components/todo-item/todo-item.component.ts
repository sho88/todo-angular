import { ITodoItem } from './../../interfaces/todo.interface';
import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TodoItemComponent {
  @Input() data: ITodoItem;
  @Output() remove: EventEmitter<ITodoItem> = new EventEmitter<ITodoItem>();
  @Output() toggled: EventEmitter<ITodoItem> = new EventEmitter<ITodoItem>();

  // events go here...
  onChange(value: boolean): void {
    this.toggled.emit({
      ...this.data,
      completed: value
    });
  }

  onClick(): void {
    this.remove.emit(this.data);
  }
}
