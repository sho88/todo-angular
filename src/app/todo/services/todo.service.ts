import { ITodoItem } from './../interfaces/todo.interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  readonly source: BehaviorSubject<ITodoItem[]> = new BehaviorSubject<ITodoItem[]>([]);
  readonly state$: Observable<ITodoItem[]> = this.source.asObservable();

  addItem(item: ITodoItem): void {
    this.source.next([ ...this.source.value, {
      _id: this.source.value.length,
      ...item,
      completed: false,
    }]);
  }

  toggleItem(item: ITodoItem): void {
    this.source.next(this.source.value.map((todoItem: ITodoItem) => (
      item._id === todoItem._id ? item : todoItem
    )));
  }

  removeItem(item: ITodoItem): void {
    this.source.next(this.source.value.filter(todoItem => todoItem._id !== item._id));
  }

  // responsible for giving me the state
  getState(): Observable<ITodoItem[]> {
    return this.state$;
  }
}
