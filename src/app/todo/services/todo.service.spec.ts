import { TestBed } from '@angular/core/testing';

import { TodoService } from './todo.service';

describe('TodoService', () => {
  let service: TodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TodoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('WHEN I add an item', () => {
    it('SHOULD add the item into the source', () => {
      spyOn(service.source, 'next');
      const item = { title: 'Testing with Dare' };
      service.addItem(item);

      expect(service.source.next).toHaveBeenCalled();
    });
  });
});
