import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoInputComponent } from './components/todo-input/todo-input.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [TodoItemComponent, TodoListComponent, TodoInputComponent],
  imports: [CommonModule, FormsModule],
  exports: [TodoListComponent, TodoInputComponent]
})
export class TodoModule { }
